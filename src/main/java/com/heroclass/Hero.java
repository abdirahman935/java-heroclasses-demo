package com.heroclass;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Hero abstract class to encapsulate shared functionality
public abstract class Hero {
    protected String name;
    protected int level;
    protected HeroAttribute levelAttributes;
    protected Map<Slot, Item> equipment;
    protected List<WeaponType> validWeaponTypes;
    protected List<ArmorType> validArmorTypes;

    public Hero(String name) {
        this.name = name;
        this.level = 1;
        this.levelAttributes = new HeroAttribute(0, 0, 0);
        this.equipment = new HashMap<>();
        for (Slot slot : Slot.values()) {
            this.equipment.put(slot, null);
        }
        this.validWeaponTypes = new ArrayList<>();
        this.validArmorTypes = new ArrayList<>();
    }

    public void levelUp() {
        level++;
        HeroAttribute gain = levelUpAttributeGain();
        levelAttributes.add(gain);
    }

    public void equip(Weapon weapon) {
        if (!validWeaponTypes.contains(weapon.getType())) {
            throw new IllegalArgumentException("Cannot equip this type of weapon");
        }
        equipment.put(Slot.Weapon, weapon);
    }

    public void equip(Armor armor) {
        if (!validArmorTypes.contains(armor.getType())) {
            throw new IllegalArgumentException("Cannot equip this type of armor");
        }
        equipment.put(armor.getSlot(), armor);
    }

    public HeroAttribute totalAttributes() {
        HeroAttribute totalAttributes = new HeroAttribute(levelAttributes.getStrength(), levelAttributes.getDexterity(),
                levelAttributes.getIntelligence());
        for (Item item : equipment.values()) {
            if (item != null && item instanceof Armor) {
                totalAttributes.add(((Armor) item).getArmorAttribute());
            }
        }
        return totalAttributes;
    }

    public abstract HeroAttribute levelUpAttributeGain();

    public abstract void display();


}
