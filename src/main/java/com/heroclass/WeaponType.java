package com.heroclass;

// WeaponType enum to represent weapon types
enum WeaponType {
    AXE, BOW, DAGGER, HAMMER, STAFF, SWORD, WAND
}
