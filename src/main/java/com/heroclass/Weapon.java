package com.heroclass;

// Weapon class to represent weapons
public class Weapon extends Item {
    private WeaponType type;
    private int damage;

    public Weapon(String name, int requiredLevel, WeaponType type, int damage) {
        super(name, requiredLevel, Slot.Weapon);
        this.type = type;
        this.damage = damage;
    }

    public WeaponType getType() {
        return type;
    }

    public int getDamage() {
        return damage;
    }
}
