package com.heroclass;

public class Ranger extends Hero {
    public Ranger(String name) {
        super(name);
        this.validWeaponTypes.add(WeaponType.BOW);
        this.validArmorTypes.add(ArmorType.LEATHER);
        this.validArmorTypes.add(ArmorType.MAIL);
        this.levelAttributes = new HeroAttribute(1, 7, 1);
    }

    @Override
    public HeroAttribute levelUpAttributeGain() {
        return new HeroAttribute(1, 5, 1);
    }

    @Override
    public void display() {
        System.out.println("Ranger: " + name + " (Level " + level + ")");
    }
}
