package com.heroclass;

public class Rogue extends Hero {
    public Rogue(String name) {
        super(name);
        this.validWeaponTypes.add(WeaponType.DAGGER);
        this.validWeaponTypes.add(WeaponType.SWORD);
        this.validArmorTypes.add(ArmorType.LEATHER);
        this.validArmorTypes.add(ArmorType.MAIL);
        this.levelAttributes = new HeroAttribute(2, 6, 1);
    }

    @Override
    public HeroAttribute levelUpAttributeGain() {
        return new HeroAttribute(1, 4, 1);
    }

    @Override
    public void display() {
        System.out.println("Rogue: " + name + " (Level " + level + ")");
    }
}


//    public void dealDamageTo(Hero other) {
//        other.takeDamage(other);
//    }
//
//    public void takeDamage(int damage) {
//        name = name - Math.max(0, damage-strength);
//    }
