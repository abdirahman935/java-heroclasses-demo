package com.heroclass;

// ArmorType enum to represent ArmorType types
enum ArmorType {
    CLOTH, LEATHER, MAIL, PLATE
}
