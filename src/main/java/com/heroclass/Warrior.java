package com.heroclass;



public class Warrior extends Hero {
    public Warrior(String name) {
        super(name);
        this.validWeaponTypes.add(WeaponType.AXE);
        this.validWeaponTypes.add(WeaponType.HAMMER);
        this.validWeaponTypes.add(WeaponType.SWORD);
        this.validArmorTypes.add(ArmorType.MAIL);
        this.validArmorTypes.add(ArmorType.PLATE);
        this.levelAttributes = new HeroAttribute(5, 2, 1);
    }

    @Override
    public HeroAttribute levelUpAttributeGain() {
        return new HeroAttribute(3, 2, 1);
    }

    @Override
    public void display() {
        System.out.println("Mage: " + name + " (Level " + level + ")");
    }
}
