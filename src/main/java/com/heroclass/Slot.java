package com.heroclass;

// Slot enum to represent body & weapon types
public enum Slot {
    Weapon, Head, Body, Legs
}
