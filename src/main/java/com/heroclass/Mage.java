package com.heroclass;

public class Mage extends Hero {
    public Mage(String name) {
        super(name);
        this.validWeaponTypes.add(WeaponType.STAFF);
        this.validWeaponTypes.add(WeaponType.WAND);
        this.validArmorTypes.add(ArmorType.CLOTH);
        this.levelAttributes = new HeroAttribute(1, 1, 8);
    }

    @Override
    public HeroAttribute levelUpAttributeGain() {
        return new HeroAttribute(1, 1, 5);
    }

    @Override
    public void display() {
        System.out.println("Mage: " + name + " (Level " + level + ")");
    }
}
