package com.heroclass;

// Armor class to represent armor type
public class Armor extends Item {
    private ArmorType type;
    private HeroAttribute armorAttribute;

    public Armor(String name, int requiredLevel, ArmorType type, HeroAttribute armorAttribute) {
        super(name, requiredLevel, getSlotForArmorType(type));
        this.type = type;
        this.armorAttribute = armorAttribute;
    }

    public ArmorType getType() {
        return type;
    }

    public HeroAttribute getArmorAttribute() {
        return armorAttribute;
    }

    private static Slot getSlotForArmorType(ArmorType type) {
        switch (type) {
            case CLOTH:
                return Slot.Body;
            case LEATHER:
                return Slot.Body;
            case MAIL:
                return Slot.Body;
            case PLATE:
                return Slot.Body;
            default:
                return null;
        }
    }
}
