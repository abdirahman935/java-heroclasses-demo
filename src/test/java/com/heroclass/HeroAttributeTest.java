package com.heroclass;
//This test has two methods: testAdd() and testIncrease(). testAdd() tests the add()
// method of the HeroAttribute class, which takes another HeroAttribute object as an argument, adds
// the corresponding attribute values of both objects and returns the sum. testIncrease() tests the
// increase() method of the HeroAttribute class, which increases each attribute value by the corresponding argument values.



import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HeroAttributeTest {

    @Test
    public void testAdd() {
        HeroAttribute attr1 = new HeroAttribute(2, 3, 4);
        HeroAttribute attr2 = new HeroAttribute(1, 2, 3);
        HeroAttribute expectedAttr = new HeroAttribute(3, 5, 7);
        attr1.add(attr2);
        assertEquals(expectedAttr.getStrength(), attr1.getStrength());
        assertEquals(expectedAttr.getDexterity(), attr1.getDexterity());
        assertEquals(expectedAttr.getIntelligence(), attr1.getIntelligence());
    }

    @Test
    public void testIncrease() {
        HeroAttribute attr = new HeroAttribute(2, 3, 4);
        int strength = 1, dexterity = 2, intelligence = 3;
        HeroAttribute expectedAttr = new HeroAttribute(3, 5, 7);
        attr.increase(strength, dexterity, intelligence);
        assertEquals(expectedAttr.getStrength(), attr.getStrength());
        assertEquals(expectedAttr.getDexterity(), attr.getDexterity());
        assertEquals(expectedAttr.getIntelligence(), attr.getIntelligence());
    }
}

