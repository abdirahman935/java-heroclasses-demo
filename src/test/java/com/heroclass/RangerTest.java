package com.heroclass;
//created a Ranger object named "Aragorn" and checks if its initial attributes,
// such as name, level, validWeaponTypes, validArmorTypes, and levelAttributes are set correctly.
// Then, the test calls the "levelUp" method of the Ranger object and checks if the level and levelAttributes have been updated correctly

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RangerTest {
    @Test
    public void testRanger() {
        Ranger ranger = new Ranger("Aragorn");
        assertEquals("Aragorn", ranger.name);
        assertEquals(1, ranger.level);
        assertEquals(1, ranger.validWeaponTypes.size());
        assertEquals(2, ranger.validArmorTypes.size());
        assertEquals(1, ranger.levelAttributes.getStrength());
        assertEquals(7, ranger.levelAttributes.getDexterity());
        assertEquals(1, ranger.levelAttributes.getIntelligence());

        ranger.levelUp();
        assertEquals(2, ranger.level);
        assertEquals(2, ranger.levelAttributes.getStrength());
        assertEquals(12, ranger.levelAttributes.getDexterity());
        assertEquals(2, ranger.levelAttributes.getIntelligence());
    }
}