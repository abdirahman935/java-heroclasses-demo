package com.heroclass;
// created a Rogue object named "TestRogue" and saves its initial level and attributes.
// Then, the test calls the "levelUp" method of the Rogue object and saves its new level and attributes.
// Finally, the test checks if the level and attributes have been updated correctly by ensuring that the new level is one greater than the initial level
// and the new attributes have increased by the correct amount.

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RogueTest {

    @Test
    public void testRogueLevelUp() {
        Rogue rogue = new Rogue("TestRogue");
        int initialLevel = rogue.level;
        HeroAttribute initialAttributes = rogue.totalAttributes();

        rogue.levelUp();
        int newLevel = rogue.level;
        HeroAttribute newAttributes = rogue.totalAttributes();

        assertEquals(initialLevel + 1, newLevel);
        assertEquals(initialAttributes.getStrength() + 1, newAttributes.getStrength());
        assertEquals(initialAttributes.getDexterity() + 4, newAttributes.getDexterity());
        assertEquals(initialAttributes.getIntelligence() + 1, newAttributes.getIntelligence());
    }
}