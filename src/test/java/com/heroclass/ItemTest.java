package com.heroclass;
//This test method creates an instance of the Weapon class which extends Item, and asserts that the
// constructor of the Item class sets the correct values for the name, requiredLevel, and slot fields.


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ItemTest {
    @Test
    public void testItemConstructor() {
        Item item = new Weapon("Excalibur", 5, WeaponType.SWORD, 10);
        assertEquals("Excalibur", item.getName());
        assertEquals(5, item.getRequiredLevel());
        assertEquals(Slot.Weapon, item.getSlot());
    }
}