package com.heroclass;
// This test creates an instance of the Weapon class and checks that its methods return the expected values.

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class WeaponTest {

    @Test
    public void testWeapon() {
        Weapon weapon = new Weapon("Sword of Truth", 10, WeaponType.SWORD, 20);

        assertEquals("Sword of Truth", weapon.getName());
        assertEquals(10, weapon.getRequiredLevel());
        assertEquals(Slot.Weapon, weapon.getSlot());
        assertEquals(WeaponType.SWORD, weapon.getType());
        assertEquals(20, weapon.getDamage());
    }
}