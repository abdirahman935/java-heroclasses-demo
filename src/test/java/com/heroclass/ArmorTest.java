package com.heroclass;
//This test checks the getters of the Armor class by creating an instance of the class
// and comparing the values returned by the getters with the expected values.
// The assertEquals method is used to check that each value matches the expected value.

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ArmorTest {

    @Test
    public void testGetters() {
        ArmorType type = ArmorType.CLOTH;
        HeroAttribute attribute = new HeroAttribute(1, 2, 3);
        Armor armor = new Armor("Cloth Armor", 1, type, attribute);

        assertEquals("Cloth Armor", armor.getName());
        assertEquals(1, armor.getRequiredLevel());
        assertEquals(Slot.Body, armor.getSlot());
        assertEquals(type, armor.getType());
        assertEquals(attribute, armor.getArmorAttribute());
    }
}