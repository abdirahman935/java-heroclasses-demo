package com.heroclass;

//This unit test includes assertions for each of the methods in the Hero class,
//in this example test, i'm using a concrete implementation of the Hero class that
// implements the abstract methods levelUpAttributeGain() and display()

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;



    public class HeroTest {

        private Hero hero;

        @Before
        public void setUp() {
            hero = new Hero("TestHero") {
                @Override
                public HeroAttribute levelUpAttributeGain() {
                    return new HeroAttribute(2, 2, 2);
                }

                @Override
                public void display() {
                    System.out.println("Test Hero");
                }
            };
        }

        @Test
        public void testLevelUp() {
            hero.levelUp();
            assertEquals(2, hero.level);
            assertEquals(2, hero.totalAttributes().getStrength());
            assertEquals(2, hero.totalAttributes().getDexterity());
            assertEquals(2, hero.totalAttributes().getIntelligence());
        }

        @Test
        public void testEquipWeapon() {
            Weapon weapon = new Weapon("Sword",5, WeaponType.SWORD, 10);
            hero.validWeaponTypes.add(WeaponType.SWORD);
            hero.equip(weapon);
            assertEquals(weapon, hero.equipment.get(Slot.Weapon));
        }

        @Test
        public void testEquipArmor() {
            Armor armor = new Armor("plate", 10, ArmorType.MAIL, new HeroAttribute(5, 5, 5));
            hero.validArmorTypes.add(ArmorType.MAIL);
            hero.equip(armor);
            assertEquals(armor, hero.equipment.get(Slot.Body));
        }
    }