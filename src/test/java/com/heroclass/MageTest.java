package com.heroclass;
//first test method "testValidWeaponTypes()" checks if the Mage object is created with the valid weapon types and correctly returns true for staff and wand weapons,
// and false for sword and axe weapons.
//second test method "testValidArmorTypes()" checks if the Mage object is created with the valid armor types and
// correctly returns true for cloth armor, and false for leather, mail, and plate armor types.
// third test method "testLevelUpAttributeGain()" tests if the levelUpAttributeGain() method of the Mage class returns the expected HeroAttribute object.
// The fourth test method "testMageEquipStaff()" tests if the Mage object can equip a staff weapon.

import org.junit.Test;
import static org.junit.Assert.*;



public class MageTest {

    @Test
    public void testValidWeaponTypes() {
        Mage mage = new Mage("Andal");
        assertTrue(mage.validWeaponTypes.contains(WeaponType.STAFF));
        assertTrue(mage.validWeaponTypes.contains(WeaponType.WAND));
        assertFalse(mage.validWeaponTypes.contains(WeaponType.SWORD));
        assertFalse(mage.validWeaponTypes.contains(WeaponType.AXE));
    }

    @Test
    public void testValidArmorTypes() {
        Mage mage = new Mage("Andal");
        assertTrue(mage.validArmorTypes.contains(ArmorType.CLOTH));
        assertFalse(mage.validArmorTypes.contains(ArmorType.LEATHER));
        assertFalse(mage.validArmorTypes.contains(ArmorType.MAIL));
        assertFalse(mage.validArmorTypes.contains(ArmorType.PLATE));
    }

    @Test
    public void testLevelUpAttributeGain() {
        Mage mage = new Mage("Andal");
        HeroAttribute expected = new HeroAttribute(1, 1, 5);
        HeroAttribute actual = mage.levelUpAttributeGain();
        assertEquals(expected, actual);
    }
    @Test
        public void testMageEquipStaff() {
        Mage mage = new Mage("Andal");
        Weapon staff = new Weapon("Staff of Power", 1, WeaponType.STAFF, 10);
        mage.equip(staff);
        org.junit.Assert.assertEquals(staff, mage.equipment.get(Slot.Weapon));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMageEquipSword() {
        Mage mage = new Mage("Andal");
        Weapon sword = new Weapon("Sword of Justice", 1, WeaponType.SWORD, 10);
        mage.equip(sword);
    }
}




